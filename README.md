# UB Reader

A thrown together script that reads a FunkeyID from a U.B. Funkey Hub.

## Dependncies

### Debian

You may have to add your user to the dialout group if you see a permission denied error.

    $ apt install python3 python3-pip
    $ pip3 install libusb

## Running

    $ python3 ./read-from-hub.py

## Protocol

To the best of my knowledge the protol looks like this:

    > URB Control (host to device)
    < Response data: 00 00 00 00
    > URB Interrupt (host to device)
    < Reponse data: 00 00 00 00 00 00 00

The interrupt response is pretty inconsistent but if you run the program a few times it settles on a consistent value.
When the reader is empty you can expect the return to be `ff ff ff ff ff 00 00`.

TODO: Post pcap

## License

This is heavily inspired by the examples in [libusb](https://github.com/karpierz/libusb/blob/8c4dfc3ee114684f62dbb5b77a02a0ff2383dd91/src/libusb/_libusb.py) this is licensed under the GNU Lesser GPLv3:

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
