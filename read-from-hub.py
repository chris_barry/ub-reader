import sys
import errno
import ctypes as ct
import libusb as usb

EP_INTR  = 1 | usb.LIBUSB_ENDPOINT_IN
EP_DATA  = 2 | usb.LIBUSB_ENDPOINT_IN
CTRL_IN  = usb.LIBUSB_REQUEST_TYPE_CLASS | usb.LIBUSB_ENDPOINT_IN
CTRL_OUT = usb.LIBUSB_REQUEST_TYPE_CLASS | usb.LIBUSB_ENDPOINT_OUT
USB_RQ   = 0x00
VID      = 0x0e4c
PID      = 0x7288
class_id = usb.LIBUSB_HOTPLUG_MATCH_ANY
RESP     = 7
global devh

#@annotate(int)
def print_f0_data():

    global devh

    data = (ct.c_ubyte * 4)(*[0])
    r = usb.control_transfer(devh, CTRL_IN, 0x0, 0x0, 0, data, ct.sizeof(data), 0)
    if r < 0:
        print("F0 error {}".format(usb.error_name(r)), file=sys.stderr)
        return r
    if r < ct.sizeof(data):
        print("short read ({})".format(r), file=sys.stderr)
        return -1

    print("F0 data:", end="")
    for i in range(ct.sizeof(data)):
        print("{:02x} ".format(data[i]), end="")
    print()
    return 0

#@annotate(data=ct.POINTER(ct.c_ubyte))
def do_sync_intr(data):

    global devh

    transferred = ct.c_int()
    r = usb.interrupt_transfer(devh, EP_INTR, data, RESP, ct.byref(transferred), 1000)
    if r < 0:
        print("intr error {}".format(r), file=sys.stderr)
        return r
    if transferred.value < RESP:
        print("short read ({}) expected {}".format(r,transferred.value), file=sys.stderr)
        return -1

    # need loop
    for i in range(ct.sizeof(data)):
        print("{:02x} ".format(data[i]), end="")
    print()
    return 0

#@annotate(int)
def find_dpfp_device():

    global devh
    global VID, PID
    devh = usb.open_device_with_vid_pid(None, VID, PID)
    return 0 if devh else -errno.EIO

def main():
    global devh

    r = usb.init(None)
    if r < 0:
        print("Failed to initialise libusb", file=sys.stderr)
        sys.exit(1)

    r = find_dpfp_device()
    try:
        if r < 0:
            print("Could not find/open device", file=sys.stderr)
            return abs(r)
        r = usb.claim_interface(devh, 0)
        if r < 0:
            print("usb_claim_interface error {}".format(r), file=sys.stderr)
            return abs(r)
        print("claimed interface")

        r = print_f0_data()
        if r < 0:
            usb.release_interface(devh, 0)
            return abs(r)
        data = (ct.c_ubyte * RESP)()
        r = do_sync_intr(data)
    finally:
        usb.release_interface(devh, 0)
    
sys.exit(main() or 0)

